import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/speech_api.m.dart';

Future<CardAPIResult> getFlashCards(
    {required LanguageMode language, required DiffcultyMode difficulty}) async {
  final timestamp = DateTime.now().millisecondsSinceEpoch;

  final res = await http.get(Uri.parse(
      'https://api.openrussian.org/api/trainer/flashcards/next?source=${difficulty.value}&cards=&excludeWordIds=&dummy=$timestamp&lang=${language.value}'));

  final body = jsonDecode(res.body);

  final result = body['result'];

  final List<CardModel> cards = [];
  List<dynamic> cardsDynamic = result['cards'];

  for (final c in cardsDynamic) {
    final tlGroups = getTLS(c['tlGroups']);

    final sentenceDynamic = c['sentence'];

    CardSentenceModel? sentence;

    if (sentenceDynamic != null) {
      sentence = CardSentenceModel(
          ru: sentenceDynamic['ru'],
          ru2: sentenceDynamic['ru2'],
          tl: sentenceDynamic['tl']);
    }

    final cM = CardModel(
        ruAccented: c['ruAccented'],
        ruAccentedHtml: c['ruAccentedHtml'],
        ruBare: c['ruBare'],
        tlGroups: tlGroups,
        type: c['type'],
        wordId: c['wordId'],
        sentence: sentence);

    cards.add(cM);
  }

  final apiRes = CardAPIResult(cards: cards);

  return apiRes;
}

Future<SuggestionsAPIResult> getSuggestions(
    {required String text, required LanguageMode language}) async {
  final encodedText = Uri.encodeFull(text);

  final timestamp = DateTime.now().millisecondsSinceEpoch;

  final res = await http.get(Uri.parse(
      'https://api.openrussian.org/suggestions?q=$encodedText&dummy=$timestamp&lang=${language.value}'));

  final body = jsonDecode(res.body);

  final result = body['result'];

  List<WordModel> words = [];
  List<dynamic> wordsDynamic = result['words'];

  for (final w in wordsDynamic) {
    final tls = getTLS(w['tls']);

    words.add(WordModel(
      id: w['id'],
      ru: w['ru'],
      tls: tls,
      type: w['type'],
    ));
  }

  List<dynamic> formOfDynamic = result['formOf'];
  List<FormModel> forms = [];

  for (final fD in formOfDynamic) {
    final formSourceDynamic = fD['source'];

    final tls = getTLS(formSourceDynamic['tls']);

    WordModel formSource = WordModel(
      id: formSourceDynamic['id'],
      ru: formSourceDynamic['ru'],
      tls: tls,
      type: formSourceDynamic['type'],
    );

    final fM = FormModel(
      form: fD['form'],
      formType: fD['formType'],
      formSource: formSource,
    );

    forms.add(fM);
  }

  final apiRes = SuggestionsAPIResult(
    words: words,
    term: result['term'],
    formOf: forms,
  );

  return apiRes;
}

List<List<String>> getTLS(List<dynamic> tlsDynamic) {
  List<List<String>> tls = [];

  for (final t in tlsDynamic) {
    List<String> tS = [];
    for (final tX in t) {
      tS.add(tX);
    }
    tls.add(tS);
  }
  return tls;
}
