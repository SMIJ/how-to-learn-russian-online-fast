import 'dart:convert';

import 'package:htlrf/backend/models/vocable.m.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/speech_api.m.dart';

Future<bool> getDailyReviseIsFinished() async {
  final prefs = await SharedPreferences.getInstance();
  final d = prefs.getBool('dailyReviseIsFinished');

  if (d == null) {
    return false;
  } else {
    return d;
  }
}

Future<bool> setDailyReviseIsFinished(bool value) async {
  final prefs = await SharedPreferences.getInstance();
  return (await prefs.setBool('dailyReviseIsFinished', value));
}

Future<int> getDailyReviseTimestamp() async {
  final prefs = await SharedPreferences.getInstance();
  int? d = prefs.getInt('dailyReviseTimestamp');

  d ??= 0;

  return d;
}

Future<bool> setDailyReviseTimestamp(int timestamp) async {
  final prefs = await SharedPreferences.getInstance();
  return (await prefs.setInt('dailyReviseTimestamp', timestamp));
}

Future<bool> getDailyWordIsFinished() async {
  final prefs = await SharedPreferences.getInstance();
  final d = prefs.getBool('dailyWordIsFinished');

  if (d == null) {
    return false;
  } else {
    return d;
  }
}

Future<bool> setDailyWordIsFinished(bool value) async {
  final prefs = await SharedPreferences.getInstance();
  return (await prefs.setBool('dailyWordIsFinished', value));
}

Future<int> getDailyWordTimestamp() async {
  final prefs = await SharedPreferences.getInstance();
  int? d = prefs.getInt('dailyWordTimestamp');

  d ??= 0;

  return d;
}

Future<bool> setDailyWordTimestamp(int timestamp) async {
  final prefs = await SharedPreferences.getInstance();
  return (await prefs.setInt('dailyWordTimestamp', timestamp));
}

Future<bool> increaseDailyWordCounter() async {
  final prefs = await SharedPreferences.getInstance();
  int? c = prefs.getInt('dailyWordCounter');

  c ??= 0;
  c++;

  return await prefs.setInt('dailyWordCounter', c);
}

Future<bool> resetDailyWordCounter() async {
  final prefs = await SharedPreferences.getInstance();

  return await prefs.setInt('dailyWordCounter', 0);
}

Future<int> getDailyWordCounter() async {
  final prefs = await SharedPreferences.getInstance();
  int? c = prefs.getInt('dailyWordCounter');

  c ??= 0;
  return c;
}

Future<List<String>> getUserWordList() async {
  final prefs = await SharedPreferences.getInstance();
  List<String>? s = prefs.getStringList('userWordList');

  s ??= [];

  return s;
}

Future<bool> addUserWordList(Vocable cardModel) async {
  final prefs = await SharedPreferences.getInstance();

  final cM = jsonEncode(cardModel);

  List<String> wordList = await getUserWordList();

  wordList.add(cM);

  return (await prefs.setStringList('userWordList', wordList));
}

Future<bool> setUserWordList(List<String> stringList) async {
  final prefs = await SharedPreferences.getInstance();

  return (await prefs.setStringList('userWordList', stringList));
}

Future<bool> addTimestampToUserWordList(int wordId, int timestamp) async {
  List<Vocable> vocList = vocableFromStringList(await getUserWordList());

  List<String> jsonList = [];

  for (final v in vocList) {
    if (v.card.wordId == wordId) {
      v.history.add(timestamp);
    }
    jsonList.add(jsonEncode(v));
  }
  return setUserWordList(jsonList);
}

List<Vocable> removeDuplicates(List<Vocable> cardModels) {
  return cardModels.toSet().toList();
}

List<Vocable> vocableFromStringList(List<String> list) {
  List<Vocable> cM = [];

  for (final s in list) {
    final card = Vocable.fromJson(jsonDecode(s));

    cM.add(card);
  }

  return cM;
}
