import 'dart:math';

import 'package:htlrf/backend/models/vocable.m.dart';

class VocableScrambler {
  VocableScrambler(
      {required this.vocables, required this.maxDays, required this.maxRevise});

  List<Vocable> vocables;
  int maxRevise;
  int maxDays;

  Vocable? get vocable {
    // |timestamp.length=0 => poolStock=poolSize

    // maxPoolStock =
    // |timestamp.length<maxRevise => poolStock=vocables[i].history.length / maxRevisitions

    // |timestamp.length>=maxRevise => poolStock=0

    int maxPoolSize = 0;

    for (final v in vocables) {
      int a;

      if (v.history.length < maxRevise) {
        a = v.history.length;
      } else if (v.history.length > maxRevise) {
        a = maxRevise;
      } else if (DateTime.now()
              .subtract(Duration(days: maxDays))
              .millisecondsSinceEpoch <
          v.history.first) {
        a = 1;
      } else {
        a = 0;
      }

      maxPoolSize += a;
    }
    double poolSizeFactor;
    if (maxPoolSize > 0) {
      poolSizeFactor = (2000 / (maxPoolSize));
    } else {
      poolSizeFactor = 0;
    }

    List<int> pool = [];

    //fill the pool
    for (int i = 0; i < vocables.length; i++) {
      int poolStock = 0;

      if (vocables[i].history.length == 1) {
        // poolStock = maxRevisitions;
        pool.clear();
        pool.add(i);
        break;
      } else if (vocables[i].history.length < maxRevise) {
        poolStock =
            ((1 - (vocables[i].history.length / maxRevise)) * poolSizeFactor)
                .round();

        for (int j = 0; j < poolStock; j++) {
          pool.add(i);
        }
      } else if (DateTime.now()
              .subtract(Duration(days: maxDays))
              .millisecondsSinceEpoch <
          vocables[i].history.first) {
        pool.add(i);
      } else if (vocables[i].history.length >= maxRevise) {
        poolStock = 0;
      }
    }
    if (pool.isNotEmpty) {
      // print("pool.length ${pool.length}");
      // print("pool $pool");

      final randomIndex = Random().nextInt(pool.length);

      final index = pool.elementAt(randomIndex);

      final voc = vocables[index];
      vocables.removeAt(index);
      return voc;
    } else {
      return null;
    }
  }
}
