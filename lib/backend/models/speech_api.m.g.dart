// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'speech_api.m.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuggestionsAPIResult _$SuggestionsAPIResultFromJson(
        Map<String, dynamic> json) =>
    SuggestionsAPIResult(
      words: (json['words'] as List<dynamic>)
          .map((e) => WordModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      term: json['term'] as String,
      derivates: json['derivates'] as List<dynamic>?,
      formOf: (json['formOf'] as List<dynamic>?)
          ?.map((e) => FormModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      number: json['number'],
      sentences: (json['sentences'] as List<dynamic>?)
          ?.map((e) => SentenceModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SuggestionsAPIResultToJson(
        SuggestionsAPIResult instance) =>
    <String, dynamic>{
      'words': instance.words,
      'term': instance.term,
      'derivates': instance.derivates,
      'formOf': instance.formOf,
      'sentences': instance.sentences,
      'number': instance.number,
    };

WordModel _$WordModelFromJson(Map<String, dynamic> json) => WordModel(
      id: json['id'] as int,
      ru: json['ru'] as String,
      tls: (json['tls'] as List<dynamic>)
          .map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      type: json['type'] as String,
    );

Map<String, dynamic> _$WordModelToJson(WordModel instance) => <String, dynamic>{
      'id': instance.id,
      'ru': instance.ru,
      'type': instance.type,
      'tls': instance.tls,
    };

FormModel _$FormModelFromJson(Map<String, dynamic> json) => FormModel(
      form: json['form'] as String,
      formSource:
          WordModel.fromJson(json['formSource'] as Map<String, dynamic>),
      formType: json['formType'] as String,
    );

Map<String, dynamic> _$FormModelToJson(FormModel instance) => <String, dynamic>{
      'form': instance.form,
      'formType': instance.formType,
      'formSource': instance.formSource,
    };

SentenceModel _$SentenceModelFromJson(Map<String, dynamic> json) =>
    SentenceModel(
      id: json['id'] as int,
      links: (json['links'] as List<dynamic>)
          .map((e) => LinkModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      ru: json['ru'] as String,
    );

Map<String, dynamic> _$SentenceModelToJson(SentenceModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'ru': instance.ru,
      'links': instance.links,
    };

LinkModel _$LinkModelFromJson(Map<String, dynamic> json) => LinkModel(
      start: json['start'] as int,
      length: json['length'] as int,
      word: json['word'] as String,
    );

Map<String, dynamic> _$LinkModelToJson(LinkModel instance) => <String, dynamic>{
      'start': instance.start,
      'length': instance.length,
      'word': instance.word,
    };

CardAPIResult _$CardAPIResultFromJson(Map<String, dynamic> json) =>
    CardAPIResult(
      cards: (json['cards'] as List<dynamic>)
          .map((e) => CardModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      queued: json['queued'],
    );

Map<String, dynamic> _$CardAPIResultToJson(CardAPIResult instance) =>
    <String, dynamic>{
      'cards': instance.cards,
      'queued': instance.queued,
    };

CardModel _$CardModelFromJson(Map<String, dynamic> json) => CardModel(
      ruAccented: json['ruAccented'] as String,
      ruAccentedHtml: json['ruAccentedHtml'] as String,
      ruBare: json['ruBare'] as String,
      tlGroups: (json['tlGroups'] as List<dynamic>)
          .map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      type: json['type'] as String,
      wordId: json['wordId'] as int,
      sentence: json['sentence'] == null
          ? null
          : CardSentenceModel.fromJson(
              json['sentence'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CardModelToJson(CardModel instance) => <String, dynamic>{
      'wordId': instance.wordId,
      'ruBare': instance.ruBare,
      'type': instance.type,
      'ruAccented': instance.ruAccented,
      'ruAccentedHtml': instance.ruAccentedHtml,
      'tlGroups': instance.tlGroups,
      'sentence': instance.sentence,
    };

CardSentenceModel _$CardSentenceModelFromJson(Map<String, dynamic> json) =>
    CardSentenceModel(
      ru: json['ru'] as String?,
      ru2: json['ru2'] as String,
      tl: json['tl'] as String,
    );

Map<String, dynamic> _$CardSentenceModelToJson(CardSentenceModel instance) =>
    <String, dynamic>{
      'ru': instance.ru,
      'ru2': instance.ru2,
      'tl': instance.tl,
    };
