// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vocable.m.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Vocable _$VocableFromJson(Map<String, dynamic> json) => Vocable(
      card: CardModel.fromJson(json['card'] as Map<String, dynamic>),
      assoziation: json['assoziation'] as String,
      history: (json['history'] as List<dynamic>).map((e) => e as int).toList(),
    );

Map<String, dynamic> _$VocableToJson(Vocable instance) => <String, dynamic>{
      'card': instance.card,
      'assoziation': instance.assoziation,
      'history': instance.history,
    };
