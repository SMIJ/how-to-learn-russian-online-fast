import 'package:htlrf/backend/models/speech_api.m.dart';

import 'package:json_annotation/json_annotation.dart';

part 'vocable.m.g.dart';

@JsonSerializable()
class Vocable {
  Vocable(
      {required this.card, required this.assoziation, required this.history});

  final CardModel card;
  final String assoziation;

  final List<int> history;

  factory Vocable.fromJson(Map<String, dynamic> json) =>
      _$VocableFromJson(json);

  Map<String, dynamic> toJson() => _$VocableToJson(this);

  @override
  bool operator ==(Object other) =>
      other is Vocable &&
      other.runtimeType == runtimeType &&
      other.card.wordId == card.wordId;

  @override
  int get hashCode => card.wordId;
}
