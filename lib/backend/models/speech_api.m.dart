import 'dart:math';

import 'package:flutter/material.dart';

import 'package:json_annotation/json_annotation.dart';

part 'speech_api.m.g.dart';

@JsonSerializable()
class SuggestionsAPIResult {
  SuggestionsAPIResult({
    required this.words,
    required this.term,
    this.derivates,
    this.formOf,
    this.number,
    this.sentences,
  });

  final List<WordModel> words;
  final String term;
  final List<dynamic>? derivates;
  final List<FormModel>? formOf;
  final List<SentenceModel>? sentences;
  dynamic number;

  factory SuggestionsAPIResult.fromJson(Map<String, dynamic> json) =>
      _$SuggestionsAPIResultFromJson(json);

  Map<String, dynamic> toJson() => _$SuggestionsAPIResultToJson(this);
}

@JsonSerializable()
class WordModel {
  WordModel({
    required this.id,
    required this.ru,
    required this.tls,
    required this.type,
  });
  final int id;
  final String ru;
  final String type;
  final List<List<String>> tls;

  factory WordModel.fromJson(Map<String, dynamic> json) =>
      _$WordModelFromJson(json);

  Map<String, dynamic> toJson() => _$WordModelToJson(this);
}

@JsonSerializable()
class FormModel {
  FormModel({
    required this.form,
    required this.formSource,
    required this.formType,
  });
  final String form;
  final String formType;
  final WordModel formSource;

  factory FormModel.fromJson(Map<String, dynamic> json) =>
      _$FormModelFromJson(json);

  Map<String, dynamic> toJson() => _$FormModelToJson(this);
}

@JsonSerializable()
class SentenceModel {
  SentenceModel({
    required this.id,
    required this.links,
    required this.ru,
  });
  final int id;
  final String ru;
  final List<LinkModel> links;

  factory SentenceModel.fromJson(Map<String, dynamic> json) =>
      _$SentenceModelFromJson(json);

  Map<String, dynamic> toJson() => _$SentenceModelToJson(this);
}

@JsonSerializable()
class LinkModel {
  LinkModel({
    required this.start,
    required this.length,
    required this.word,
  });

  final int start;
  final int length;
  final String word; //! not a String

  factory LinkModel.fromJson(Map<String, dynamic> json) =>
      _$LinkModelFromJson(json);

  Map<String, dynamic> toJson() => _$LinkModelToJson(this);
}

@JsonSerializable()
class CardAPIResult {
  CardAPIResult({required this.cards, this.queued});

  final List<CardModel> cards;
  dynamic queued;

  factory CardAPIResult.fromJson(Map<String, dynamic> json) =>
      _$CardAPIResultFromJson(json);

  Map<String, dynamic> toJson() => _$CardAPIResultToJson(this);
}

@JsonSerializable()
class CardModel {
  CardModel({
    required this.ruAccented,
    required this.ruAccentedHtml,
    required this.ruBare,
    required this.tlGroups,
    required this.type,
    required this.wordId,
    required this.sentence,
  });

  final int wordId;
  final String ruBare;
  final String type;
  final String ruAccented;
  final String ruAccentedHtml;
  final List<List<String>> tlGroups;
  final CardSentenceModel? sentence;

  factory CardModel.fromJson(Map<String, dynamic> json) =>
      _$CardModelFromJson(json);

  Map<String, dynamic> toJson() => _$CardModelToJson(this);

  @override
  bool operator ==(Object other) =>
      other is CardModel &&
      other.runtimeType == runtimeType &&
      other.wordId == wordId;

  @override
  int get hashCode => wordId;
}

@JsonSerializable()
class CardSentenceModel {
  CardSentenceModel({
    this.ru,
    required this.ru2,
    required this.tl,
  });
  final String? ru;
  final String ru2;
  final String tl;

  factory CardSentenceModel.fromJson(Map<String, dynamic> json) =>
      _$CardSentenceModelFromJson(json);

  Map<String, dynamic> toJson() => _$CardSentenceModelToJson(this);
}

enum LanguageMode {
  german('de'),
  english('en');

  const LanguageMode(this.value);
  final String value;
}

enum DiffcultyMode {
  a1('A1'),
  a2('A2'),
  b1('B1'),
  b2('B2'),
  c1('C1'),
  c2('C2');

  const DiffcultyMode(this.value);
  final String value;
}

enum CardOrientation {
  tl,
  ru;

  static CardOrientation randomOrientation() {
    if (Random().nextDouble() < 0.5) {
      return CardOrientation.tl;
    } else {
      return CardOrientation.ru;
    }
  }
}
