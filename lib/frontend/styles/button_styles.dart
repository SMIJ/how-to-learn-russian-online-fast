import 'package:flutter/material.dart';

class ButtonStyles {
  static ButtonStyle primaryStyle = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(Colors.amber),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
  static ButtonStyle secondaryStyle = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
}
