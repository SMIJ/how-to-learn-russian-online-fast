import 'package:flutter/material.dart';

class TextStyles {
  static const boldHeader1 = TextStyle(
    fontSize: 35,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );
  static const boldHeader1Secondary = TextStyle(
    fontSize: 35,
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );
  static const lightHeader1 = TextStyle(
    fontSize: 35,
    fontWeight: FontWeight.normal,
    color: Colors.amber,
  );
  static const boldHeader2 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );
  static const lightHeader2 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.normal,
    color: Colors.black,
  );
  static const lightHeader2backgroundColor = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.normal,
    color: Colors.white,
  );
  static const boldHeader2backgroundColor = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );
  static const boldHeader2primary = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.bold,
    color: Colors.amber,
  );
  static const textFieldStyle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w400,
    color: Colors.black,
  );
  static const suggestionsStyle = TextStyle(
    fontSize: 25,
    fontWeight: FontWeight.w400,
    color: Colors.black,
  );
}
