import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';

import '../../backend/models/speech_api.m.dart';

class CustomDifficultyRadioButtons extends StatefulWidget {
  const CustomDifficultyRadioButtons(
      {super.key, required this.difficulty, required this.callback});

  final DiffcultyMode difficulty;
  final void Function(DiffcultyMode? value) callback;

  @override
  State<CustomDifficultyRadioButtons> createState() =>
      _CustomDifficultyRadioButtonsState();
}

class _CustomDifficultyRadioButtonsState
    extends State<CustomDifficultyRadioButtons> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Column(
          children: [
            const Text(
              'A1',
              style: TextStyles.lightHeader2backgroundColor,
            ),
            SizedBox(
              width: 50,
              height: 50,
              child: Radio<DiffcultyMode>(
                value: DiffcultyMode.a1,
                groupValue: widget.difficulty,
                onChanged: widget.callback,
                fillColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Text(
              'A2',
              style: TextStyles.lightHeader2backgroundColor,
            ),
            SizedBox(
              width: 50,
              height: 50,
              child: Radio<DiffcultyMode>(
                value: DiffcultyMode.a2,
                groupValue: widget.difficulty,
                onChanged: widget.callback,
                fillColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Text(
              'B1',
              style: TextStyles.lightHeader2backgroundColor,
            ),
            SizedBox(
              width: 50,
              height: 50,
              child: Radio<DiffcultyMode>(
                value: DiffcultyMode.b1,
                groupValue: widget.difficulty,
                onChanged: widget.callback,
                fillColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Text(
              'B2',
              style: TextStyles.lightHeader2backgroundColor,
            ),
            SizedBox(
              width: 50,
              height: 50,
              child: Radio<DiffcultyMode>(
                value: DiffcultyMode.b2,
                groupValue: widget.difficulty,
                onChanged: widget.callback,
                fillColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Text(
              'C1',
              style: TextStyles.lightHeader2backgroundColor,
            ),
            SizedBox(
              width: 50,
              height: 50,
              child: Radio<DiffcultyMode>(
                value: DiffcultyMode.c1,
                groupValue: widget.difficulty,
                onChanged: widget.callback,
                fillColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Text(
              'C2',
              style: TextStyles.lightHeader2backgroundColor,
            ),
            SizedBox(
              width: 50,
              height: 50,
              child: Radio<DiffcultyMode>(
                value: DiffcultyMode.c2,
                groupValue: widget.difficulty,
                onChanged: widget.callback,
                fillColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
