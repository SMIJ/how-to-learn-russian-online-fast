import 'package:flutter/material.dart';

import '../styles/button_styles.dart';

class CustomIconButton extends StatefulWidget {
  const CustomIconButton(
      {super.key, required this.icon, required this.callback});

  final IconData icon;
  final Function() callback;

  @override
  State<CustomIconButton> createState() => _CustomIconButtonState();
}

class _CustomIconButtonState extends State<CustomIconButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: widget.callback,
      style: ButtonStyles.secondaryStyle,
      child: SizedBox(
        width: 45,
        height: 60,
        child: Center(
          child: Icon(
            widget.icon,
            color: Colors.amber,
            size: 40,
          ),
        ),
      ),
    );
  }
}
