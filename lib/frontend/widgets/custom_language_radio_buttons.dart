import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';

import '../../backend/models/speech_api.m.dart';

class CustomLanguageRadioButtons extends StatefulWidget {
  const CustomLanguageRadioButtons(
      {super.key, required this.language, required this.callback});

  final LanguageMode language;
  final void Function(LanguageMode? value) callback;

  @override
  State<CustomLanguageRadioButtons> createState() =>
      _CustomLanguageRadioButtonsState();
}

class _CustomLanguageRadioButtonsState
    extends State<CustomLanguageRadioButtons> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 40),
          child: Column(
            children: [
              const Text(
                'GERMAN',
                style: TextStyles.lightHeader2backgroundColor,
              ),
              SizedBox(
                width: 50,
                height: 50,
                child: Radio<LanguageMode>(
                  value: LanguageMode.german,
                  groupValue: widget.language,
                  onChanged: widget.callback,
                  fillColor: MaterialStateProperty.all<Color>(Colors.white),
                ),
              ),
            ],
          ),
        ),
        Column(
          children: [
            const Text(
              'ENGLISH',
              style: TextStyles.lightHeader2backgroundColor,
            ),
            SizedBox(
              width: 50,
              height: 50,
              child: Radio<LanguageMode>(
                value: LanguageMode.english,
                groupValue: widget.language,
                onChanged: widget.callback,
                fillColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
