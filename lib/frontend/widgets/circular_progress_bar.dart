import 'package:flutter/material.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';
import 'package:htlrf/frontend/widgets/custom_icon_button.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class CircularProgressBar extends StatefulWidget {
  const CircularProgressBar({
    super.key,
    required this.total,
    required this.current,
  });

  final int total;
  final int current;
  // final bool isFinished;

  @override
  State<CircularProgressBar> createState() => _CircularProgressBarState();
}

class _CircularProgressBarState extends State<CircularProgressBar> {
  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
        radius: 120.0,
        animation: true,
        animationDuration: 2000,
        lineWidth: 30.0,
        percent: widget.current / widget.total,
        arcBackgroundColor: Colors.amberAccent,
        arcType: ArcType.FULL,
        circularStrokeCap: CircularStrokeCap.butt,
        backgroundColor: Colors.transparent,
        progressColor: Colors.amber,
        center:
            // widget.isFinished ?
            Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "${widget.current}/${widget.total}",
              style: TextStyles.lightHeader2,
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/random');
              },
              child: const Icon(
                Icons.add,
                color: Colors.amber,
                size: 40,
              ),
            ),
          ],
        )
        // : Text(
        //     "${widget.current}/${widget.total}",
        //     style: TextStyles.lightHeader2,
        //   ),
        );
  }
}
