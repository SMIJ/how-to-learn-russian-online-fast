import 'package:flutter/material.dart';
import 'package:htlrf/backend/models/speech_api.m.dart';
import 'package:htlrf/backend/models/vocable.m.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';

class MemoryCard extends StatefulWidget {
  const MemoryCard({super.key, this.cardOrientation, required this.vocable});

  final CardOrientation? cardOrientation;
  final Vocable vocable;

  @override
  State<MemoryCard> createState() => _MemoryCardState();
}

class _MemoryCardState extends State<MemoryCard> {
  CardOrientation cO = CardOrientation.randomOrientation();
  bool selfManage = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.cardOrientation == null) {
      selfManage = true;
      cO = CardOrientation.randomOrientation();
    } else {
      cO = widget.cardOrientation!;
    }
  }

  @override
  Widget build(BuildContext context) {
    String tl = '';

    for (int i = 0; i < widget.vocable.card.tlGroups.first.length; i++) {
      if (i != widget.vocable.card.tlGroups.first.length - 1) {
        tl += '${widget.vocable.card.tlGroups.first[i]}/';
      } else {
        tl += widget.vocable.card.tlGroups.first[i];
      }
    }

    final bodyChild = cO == CardOrientation.ru
        ? Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.black,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Text(
                      widget.vocable.card.ruBare,
                      textAlign: TextAlign.center,
                      style: TextStyles.boldHeader2backgroundColor,
                    ),
                  ),
                  widget.vocable.card.sentence != null
                      ? Text(
                          '${widget.vocable.card.sentence?.ru2}',
                          textAlign: TextAlign.center,
                          style: TextStyles.lightHeader2backgroundColor,
                        )
                      : const SizedBox.shrink(),
                ],
              ),
            ),
          )
        : Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Text(
                      tl,
                      textAlign: TextAlign.center,
                      style: TextStyles.boldHeader2,
                    ),
                  ),
                  widget.vocable.card.sentence != null
                      ? Text(
                          '${widget.vocable.card.sentence?.tl}',
                          textAlign: TextAlign.center,
                          style: TextStyles.lightHeader2,
                        )
                      : const SizedBox.shrink(),
                ],
              ),
            ),
          );

    return selfManage
        ? GestureDetector(
            onTap: () {
              if (selfManage) {
                if (cO == CardOrientation.ru) {
                  cO = CardOrientation.tl;
                } else {
                  cO = CardOrientation.ru;
                }
                setState(() {});
              }
            },
            child: bodyChild)
        : bodyChild;
  }
}
