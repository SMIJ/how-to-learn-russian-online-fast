import 'dart:ui';

import 'package:blur/blur.dart';
import 'package:flutter/material.dart';

import 'package:htlrf/frontend/styles/button_styles.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';

class CustomActionButton extends StatefulWidget {
  const CustomActionButton(
      {super.key,
      required this.callback,
      required this.title,
      required this.isFinished});

  final Function()? callback;
  final String title;
  final bool isFinished;

  @override
  State<CustomActionButton> createState() => _CustomActionButtonState();
}

class _CustomActionButtonState extends State<CustomActionButton> {
  @override
  Widget build(BuildContext context) {
    final button = ElevatedButton(
      onPressed: widget.callback,
      style: ButtonStyles.primaryStyle,
      child: SizedBox(
        width: 300,
        height: 70,
        child: Center(
          child: Text(
            widget.title,
            textAlign: TextAlign.center,
            style: TextStyles.boldHeader2backgroundColor,
          ),
        ),
      ),
    );

    return widget.isFinished
        ? Stack(
            children: [
              Blur(
                blur: 2,
                borderRadius: BorderRadius.circular(20),
                blurColor: Colors.amber,
                child: button,
              ),
              const Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                    size: 70,
                  ),
                ),
              ),
            ],
          )
        : button;
  }
}
