import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../styles/button_styles.dart';
import '../styles/text_styles.dart';

class CustomSecondaryButton extends StatefulWidget {
  const CustomSecondaryButton(
      {super.key, required this.title, required this.callback});

  final String title;
  final Function() callback;

  @override
  State<CustomSecondaryButton> createState() => _CustomSecondaryButtonState();
}

class _CustomSecondaryButtonState extends State<CustomSecondaryButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: widget.callback,
      style: ButtonStyles.secondaryStyle,
      child: SizedBox(
        width: 280,
        height: 70,
        child: Center(
          child: Text(
            widget.title,
            textAlign: TextAlign.center,
            style: TextStyles.boldHeader2primary,
          ),
        ),
      ),
    );
  }
}
