import 'package:flutter/material.dart';

import '../styles/text_styles.dart';

class CustomInputField extends StatefulWidget {
  const CustomInputField(
      {super.key,
      required this.title,
      required this.controller,
      this.onChanged});

  final String title;
  final TextEditingController controller;
  final Function(String)? onChanged;

  @override
  State<CustomInputField> createState() => _CustomInputFieldState();
}

class _CustomInputFieldState extends State<CustomInputField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Text(
            widget.title,
            style: TextStyles.boldHeader2backgroundColor,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 20, left: 20),
          child: TextField(
            controller: widget.controller,
            style: TextStyles.textFieldStyle,
            onChanged: widget.onChanged,
            decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Colors.amber,
                ),
                borderRadius: BorderRadius.circular(20),
              ),
              focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.amber)),
            ),
            cursorColor: Colors.amber,
          ),
        ),
      ],
    );
  }
}
