import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:htlrf/backend/models/speech_api.m.dart';
import 'package:htlrf/backend/models/vocable.m.dart';
import 'package:htlrf/backend/services/shared_preferences.s.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';

import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/custom_icon_button.dart';
import '../widgets/memory_card.dart';

class VocablePage extends StatefulWidget {
  const VocablePage({
    super.key,
    required this.vocable,
    required this.cardOrientation,
    required this.successCallback,
    required this.failureCallback,
  });

  final Vocable vocable;
  final CardOrientation cardOrientation;

  final Function() failureCallback;
  final Function() successCallback;

  @override
  State<VocablePage> createState() => _VocablePageState();
}

class _VocablePageState extends State<VocablePage> {
  bool flipped = false;
  late CardOrientation cardOrientation = widget.cardOrientation;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 350,
        height: 400,
        decoration: BoxDecoration(
            color: Colors.amber, borderRadius: BorderRadius.circular(5)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'DO YOU KNOW THE ANSWER?',
                  style: TextStyles.boldHeader1,
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 40, bottom: 20, left: 30, right: 30),
                child: GestureDetector(
                  onTap: () {
                    flipped = true;

                    if (cardOrientation == CardOrientation.ru) {
                      cardOrientation = CardOrientation.tl;
                    } else if (cardOrientation == CardOrientation.tl) {
                      cardOrientation = CardOrientation.ru;
                    }

                    setState(() {});
                  },
                  child: MemoryCard(
                    cardOrientation: cardOrientation,
                    vocable: widget.vocable,
                    key: UniqueKey(),
                  ),
                ),
              ),
              flipped
                  ? Padding(
                      padding: const EdgeInsets.only(bottom: 40),
                      child: Column(
                        children: [
                          Text(
                            "association: ${widget.vocable.assoziation}",
                            style: TextStyles.lightHeader2,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CustomIconButton(
                                    icon: Icons.check,
                                    callback: widget.successCallback),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CustomIconButton(
                                  icon: Icons.close,
                                  callback: widget.failureCallback,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                  : const SizedBox.shrink(),
            ],
          ),
        ),
      ),
    );
  }
}
