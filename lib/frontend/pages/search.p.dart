import 'package:flutter/material.dart';
import 'package:htlrf/backend/services/speech_api.s.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';
import 'package:htlrf/frontend/widgets/custom_input_field.dart';

import '../../backend/models/speech_api.m.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final c1 = TextEditingController();

  SuggestionsAPIResult suggestions = SuggestionsAPIResult(words: [], term: "");

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Container(
          width: 350,
          height: 700,
          color: Colors.amber,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomInputField(
                title: "Type in the word",
                controller: c1,
                onChanged: (text) async {
                  suggestions = await getSuggestions(
                      text: text, language: LanguageMode.german);

                  setState(() {});
                },
              ),
              SizedBox(
                width: double.infinity,
                height: 500,
                child: ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: suggestions.words.length,
                  itemBuilder: ((context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (ctx) {
                              return Scaffold(
                                body: ListView.builder(
                                  itemCount:
                                      suggestions.words[index].tls.length,
                                  itemBuilder: (context, j) {
                                    return Text(
                                        suggestions.words[index].tls[j].first);
                                  },
                                ),
                              );
                            },
                          ),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(2),
                        child: Text(
                          suggestions.words[index].ru,
                          textAlign: TextAlign.center,
                          style: TextStyles.suggestionsStyle,
                        ),
                      ),
                    );
                  }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
