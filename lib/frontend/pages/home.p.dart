// import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:htlrf/backend/services/shared_preferences.s.dart';
import 'package:htlrf/frontend/pages/random.p.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';
import 'package:htlrf/frontend/widgets/circular_progress_bar.dart';
import 'package:htlrf/frontend/widgets/custom_action_button.dart';
import 'package:htlrf/frontend/pages/search.p.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
//   Future<void> initPlatformState() async {
// // Configure BackgroundFetch.
//     final status = await BackgroundFetch.configure(
//         BackgroundFetchConfig(
//           minimumFetchInterval: 2,
//           forceAlarmManager: false,
//           stopOnTerminate: false,
//           startOnBoot: true,
//           enableHeadless: true,
//           requiresBatteryNotLow: false,
//           requiresCharging: false,
//           requiresStorageNotLow: false,
//           requiresDeviceIdle: false,
//           requiredNetworkType: NetworkType.NONE,
//         ),
//         _onBackgroundFetch,
//         _onBackgroundFetchTimeout);

// // Schedule backgroundfetch for the 1st time it will execute with 1000ms delay.
// // where device must be powered (and delay will be throttled by the OS).
//     BackgroundFetch.scheduleTask(TaskConfig(
//         taskId: "com.fruitycop.updatedailyword",
//         delay: 15000,
//         periodic: false,
//         stopOnTerminate: false,
//         enableHeadless: true));
//   }

//   void _onBackgroundFetchTimeout(String taskId) {
//     BackgroundFetch.finish(taskId);
//   }

//   void _onBackgroundFetch(String taskId) async {
//     if (taskId == "com.fruitycop.updatedailyword") {
//       final dW = await getDailyWordIsFinished();
//       if (dW) {
//         setDailyWordIsFinished(false);
//       }
//     }
//   }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Padding(
              padding: EdgeInsets.only(top: 40, bottom: 50),
              child: Text(
                "Learn Russian Words Online Fast",
                style: TextStyles.boldHeader1,
                textAlign: TextAlign.center,
              ),
            ),
            FutureBuilder<int>(
                future: getDailyWordCounter(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, "/wordlist");
                      },
                      child: CircularProgressBar(
                        current: snapshot.data!,
                        total: 500,
                      ),
                    );
                  } else {
                    return const CircularProgressBar(
                      current: 0,
                      total: 500,
                    );
                  }
                }),
            // Padding(
            //   padding: const EdgeInsets.only(top: 30),
            //   child: CustomActionButton(
            //       callback: () {}, title: "TRAIN WEEKLY WORDS"),
            // ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: FutureBuilder<List<dynamic>>(
                future: Future.wait([
                  getDailyWordIsFinished(),
                  getDailyWordTimestamp(),
                  getDailyReviseIsFinished(),
                  getDailyReviseTimestamp()
                ]),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    int? wordTimestamp = snapshot.data![1] as int;

                    bool isWordFinished = false;

                    if (snapshot.data![0] && wordTimestamp.isFinite) {
                      isWordFinished = DateUtils.isSameDay(
                          DateTime.fromMillisecondsSinceEpoch(wordTimestamp),
                          DateTime.now());
                    }

                    int? timestamp = snapshot.data![3] as int;

                    bool isReviseFinished = false;

                    if (snapshot.data![2] && timestamp.isFinite) {
                      isReviseFinished = DateUtils.isSameDay(
                          DateTime.fromMillisecondsSinceEpoch(timestamp),
                          DateTime.now());
                    }

                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: CustomActionButton(
                            callback: () {
                              Navigator.pushNamed(context, '/random');
                            },
                            title: "TODAY'S WORD",
                            isFinished: isWordFinished,
                          ),
                        ),
                        CustomActionButton(
                          callback: () async {
                            Navigator.pushNamed(context, '/revise');
                          },
                          title: "TODAY'S REVISE",
                          isFinished: isReviseFinished,
                        ),
                      ],
                    );
                  } else {
                    return const SizedBox.shrink();
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
