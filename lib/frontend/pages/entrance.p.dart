import 'package:flutter/material.dart';
import 'package:htlrf/frontend/widgets/custom_action_button.dart';
import 'package:htlrf/frontend/widgets/custom_input_field.dart';

import '../styles/text_styles.dart';

class EntrancePage extends StatefulWidget {
  EntrancePage({super.key});

  final c1 = TextEditingController();
  final c2 = TextEditingController();

  @override
  State<EntrancePage> createState() => _EntrancePageState();
}

class _EntrancePageState extends State<EntrancePage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          const Text(
            "Learn Russian Words Online Fast",
            style: TextStyles.boldHeader1,
          ),
          CustomInputField(title: "DAILY WORDS", controller: widget.c1),
          CustomInputField(title: "TOTAL GOAL", controller: widget.c2),
          CustomActionButton(
            callback: () {},
            title: "START",
            isFinished: false,
          )
        ],
      ),
    );
  }
}
