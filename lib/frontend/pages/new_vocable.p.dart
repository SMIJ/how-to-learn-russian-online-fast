import 'dart:math';

import 'package:flutter/material.dart';

import 'package:htlrf/backend/models/speech_api.m.dart';
import 'package:htlrf/backend/models/vocable.m.dart';
import 'package:htlrf/frontend/pages/vocable.p.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';
import 'package:htlrf/frontend/widgets/custom_input_field.dart';

import '../../backend/services/shared_preferences.s.dart';
import '../widgets/custom_icon_button.dart';

class NewVocablePage extends StatefulWidget {
  const NewVocablePage({super.key, required this.card});

  final CardModel card;

  @override
  State<NewVocablePage> createState() => _NewVocablePageState();
}

class _NewVocablePageState extends State<NewVocablePage> {
  TextEditingController c1 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 40),
        child: SingleChildScrollView(
          child: Container(
            width: 350,
            height: 600,
            color: Colors.amber,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 40),
                  child: Text(
                    "type: ${widget.card.type}",
                    style: TextStyles.lightHeader2,
                  ),
                ),
                Text(widget.card.ruBare, style: TextStyles.boldHeader1),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    "${widget.card.sentence?.ru2} ",
                    style: TextStyles.lightHeader2,
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  widget.card.tlGroups.first.first,
                  style: TextStyles.boldHeader2,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 40),
                  child: Text(
                    "${widget.card.sentence?.tl} ",
                    style: TextStyles.lightHeader2,
                    textAlign: TextAlign.center,
                  ),
                ),
                CustomInputField(title: "Association Word", controller: c1),
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: CustomIconButton(
                    icon: Icons.keyboard_arrow_right,
                    callback: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) {
                          final voc = Vocable(
                            card: widget.card,
                            assoziation: c1.text,
                            history: [DateTime.now().millisecondsSinceEpoch],
                          );

                          return Scaffold(
                            body: VocablePage(
                              vocable: voc,
                              cardOrientation:
                                  CardOrientation.randomOrientation(),
                              successCallback: () async {
                                final navigator = Navigator.of(context);

                                await setDailyWordIsFinished(true);
                                await setDailyWordTimestamp(
                                    DateTime.now().millisecondsSinceEpoch);
                                await increaseDailyWordCounter();

                                await addUserWordList(voc);

                                navigator.popAndPushNamed('/');
                              },
                              failureCallback: () {
                                Navigator.pushNamedAndRemoveUntil(
                                    context, '/random', (x) => x.isFirst);
                              },
                            ),
                          );
                        },
                      ));
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
