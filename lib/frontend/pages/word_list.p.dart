import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:confirm_dialog/confirm_dialog.dart';
import 'package:htlrf/backend/models/speech_api.m.dart';
import 'package:htlrf/backend/models/vocable.m.dart';
import 'package:htlrf/backend/services/shared_preferences.s.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';
import 'package:htlrf/frontend/widgets/memory_card.dart';

import 'package:flutter/services.dart';

import '../widgets/custom_icon_button.dart';

class WordListPage extends StatefulWidget {
  const WordListPage({super.key});

  @override
  State<WordListPage> createState() => _WordListPageState();
}

class _WordListPageState extends State<WordListPage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<String>>(
      future: getUserWordList(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Vocable> cMs = vocableFromStringList(snapshot.data!);

          cMs = removeDuplicates(cMs);

          return Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 35),
              child: Container(
                width: 375,
                color: Colors.amber,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: CustomIconButton(
                              icon: Icons.keyboard_arrow_left,
                              callback: () {
                                Navigator.pushNamedAndRemoveUntil(
                                    context, '/', (x) => false);
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: CustomIconButton(
                              icon: Icons.read_more,
                              callback: () {
                                Navigator.pushNamed(
                                  context,
                                  '/revise',
                                );
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: CustomIconButton(
                              icon: Icons.copy,
                              callback: () async {
                                await Clipboard.setData(
                                    ClipboardData(text: jsonEncode(cMs)));
                              },
                            ),
                          ),
                          CustomIconButton(
                              icon: Icons.paste,
                              callback: () async {
                                if (!(await confirm(
                                  context,
                                  title: const Text('Confirm'),
                                  content: const Text(
                                      'Would you like to paste vocables?'),
                                  textOK: const Text('Yes'),
                                  textCancel: const Text('No'),
                                ))) {
                                  return;
                                }

                                final data = await Clipboard.getData(
                                    Clipboard.kTextPlain);

                                if (data == null) return;
                                if (data.text == null) return;

                                final decodedClipboardText =
                                    jsonDecode(data.text!);

                                List<String> wordList = [];
                                await resetDailyWordCounter();

                                for (final vocable in decodedClipboardText) {
                                  wordList.add(jsonEncode(vocable));

                                  await increaseDailyWordCounter();
                                }
                                setUserWordList(wordList);

                                setState(() {});
                              })
                        ],
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        // physics: const NeverScrollableScrollPhysics(),
                        itemCount: cMs.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: MemoryCard(
                                  vocable: cMs[index],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 30),
                                child: Text(
                                  "assoziation: ${cMs[index].assoziation}",
                                  style: TextStyles.boldHeader2,
                                  textAlign: TextAlign.center,
                                ),
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
          // return const SizedBox.shrink();
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }
}
