import 'dart:math';

import 'package:flutter/material.dart';
import 'package:htlrf/backend/services/speech_api.s.dart';
import 'package:htlrf/frontend/pages/new_vocable.p.dart';
import 'package:htlrf/frontend/pages/vocable.p.dart';
import 'package:htlrf/frontend/styles/text_styles.dart';
import 'package:htlrf/frontend/widgets/custom_action_button.dart';
import 'package:htlrf/frontend/widgets/custom_input_field.dart';
import 'package:htlrf/frontend/widgets/custom_difficulty_radio_buttons.dart';
import 'package:htlrf/frontend/widgets/custom_language_radio_buttons.dart';
import 'package:htlrf/frontend/widgets/custom_secondary_button.dart';
import 'package:htlrf/frontend/widgets/memory_card.dart';

import '../../backend/models/speech_api.m.dart';

class RandomPage extends StatefulWidget {
  const RandomPage({super.key});

  @override
  State<RandomPage> createState() => _RandomPageState();
}

class _RandomPageState extends State<RandomPage> {
  DiffcultyMode difficulty = DiffcultyMode.b1;
  LanguageMode language = LanguageMode.german;
  CardAPIResult? cardApiRes;
  CardOrientation cardOrientation = CardOrientation.ru;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 350,
        height: 700,
        decoration: BoxDecoration(
            color: Colors.amber, borderRadius: BorderRadius.circular(5)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Padding(
                padding:
                    EdgeInsets.only(top: 35, bottom: 50, left: 5, right: 5),
                child: Text(
                  'CHOOSE YOUR DIFFICULTY AND LANGUAGE',
                  style: TextStyles.boldHeader1,
                  textAlign: TextAlign.center,
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 30,
                ),
                child: Text(
                  'DIFFICULTY',
                  style: TextStyles.boldHeader1Secondary,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: CustomDifficultyRadioButtons(
                  difficulty: difficulty,
                  callback: (value) {
                    if (value != null) difficulty = value;
                    setState(() {});
                  },
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 30,
                ),
                child: Text(
                  'LANGUAGE',
                  style: TextStyles.boldHeader1Secondary,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: CustomLanguageRadioButtons(
                  language: language,
                  callback: (value) {
                    if (value != null) language = value;
                    setState(() {});
                  },
                ),
              ),
              CustomSecondaryButton(
                callback: () async {
                  final navigator = Navigator.of(context);
                  final apiRes = await getFlashCards(
                      language: language, difficulty: difficulty);
                  cardApiRes = apiRes;
                  // cardOrientation = getRandomOrientation();
                  navigator.push(MaterialPageRoute(
                    builder: (context) {
                      return Scaffold(
                        body: NewVocablePage(
                          card: apiRes.cards.first,
                        ),
                      );
                    },
                  ));
                },
                title: 'GENERATE RANDOM WORD',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
