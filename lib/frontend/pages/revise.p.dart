import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:htlrf/backend/models/speech_api.m.dart';
import 'package:htlrf/backend/services/shared_preferences.s.dart';
import 'package:htlrf/backend/services/vocable_scrambler.s.dart';
import 'package:htlrf/frontend/pages/home.p.dart';
import 'package:htlrf/frontend/pages/vocable.p.dart';
import 'package:htlrf/frontend/pages/word_list.p.dart';

import '../../backend/models/vocable.m.dart';

class RevisePage extends StatefulWidget {
  const RevisePage({super.key});

  @override
  State<RevisePage> createState() => _RevisePageState();
}

class _RevisePageState extends State<RevisePage> {
  int reviseAmount = 0;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<String>>(
      future: getUserWordList(),
      builder: ((context, snapshot) {
        if (snapshot.hasData) {
          final vocables = vocableFromStringList(snapshot.data!);

          final scrambler =
              VocableScrambler(maxDays: 30, vocables: vocables, maxRevise: 20);

          final vox = scrambler.vocable;

          if (vox != null) {
            reviseAmount++;
            addTimestampToUserWordList(
                vox.card.wordId, DateTime.now().millisecondsSinceEpoch);
            return VocablePage(
                vocable: vox,
                cardOrientation: CardOrientation.randomOrientation(),
                successCallback: reviseCallback(context, scrambler),
                failureCallback: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/', (route) => false);
                });
          } else {
            Future.delayed(
              Duration(milliseconds: 1),
              () => Navigator.pushNamedAndRemoveUntil(
                  context, '/wordlist', (route) => route.isFirst),
            );
            return const SizedBox.shrink();
          }
        } else {
          return const SizedBox.shrink();
        }
      }),
    );
  }

  Function() reviseCallback(BuildContext ctx, VocableScrambler scrambler) {
    return () {
      final nav = Navigator.of(ctx);

      Vocable? vox = scrambler.vocable;
      if (vox != null && reviseAmount < 7) {
        reviseAmount++;
        addTimestampToUserWordList(
            vox.card.wordId, DateTime.now().millisecondsSinceEpoch);
        nav.pushReplacement(
          MaterialPageRoute(
            builder: (ctx) => Scaffold(
              body: VocablePage(
                  vocable: vox,
                  cardOrientation: CardOrientation.randomOrientation(),
                  successCallback: reviseCallback(ctx, scrambler),
                  failureCallback: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/', (route) => false);
                  }),
            ),
          ),
        );
      } else {
        setDailyReviseIsFinished(true);
        setDailyReviseTimestamp(DateTime.now().millisecondsSinceEpoch);

        Navigator.pushNamedAndRemoveUntil(ctx, '/', (route) => false);
      }

      // if (reviseAmount < maxRevise && vocables.isNotEmpty) {
      //   reviseAmount++;

      //   final randomIndex = Random().nextInt(vocables.length);

      //   final voc = vocables.elementAt(randomIndex);
      //   vocables.removeAt(randomIndex);

      //   nav.pushReplacement(MaterialPageRoute(
      //     builder: (ctx) => Scaffold(
      //       body: VocablePage(
      //           vocable: voc,
      //           cardOrientation: CardOrientation.randomOrientation(),
      //           successCallback: reviseCallback(ctx, vocables, maxRevise),
      //           failureCallback: () {
      //             Navigator.pushNamedAndRemoveUntil(
      //                 context, '/', (route) => false);
      //           }),
      //     ),
      //   ));
      // } else {
      //   setDailyReviseIsFinished(true);
      //   setDailyReviseTimestamp(DateTime.now().millisecondsSinceEpoch);

      //   Navigator.pushNamedAndRemoveUntil(ctx, '/', (route) => false);
      // }
    };
  }
}
