// import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:htlrf/frontend/pages/entrance.p.dart';
import 'package:htlrf/frontend/pages/home.p.dart';
import 'package:htlrf/frontend/pages/revise.p.dart';
import 'package:htlrf/frontend/pages/word_list.p.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'frontend/pages/random.p.dart';

// @pragma('vm:entry-point')
// void backgroundFetchHeadlessTask(HeadlessTask task) async {
//   if (task.timeout) {
//     BackgroundFetch.finish(task.taskId);
//     return;
//   }
// }

void main() {
  runApp(const MyApp());
  // BackgroundFetch.registerHeadlessTask(backgroundFetchHeadlessTask);
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'How to learn Russian Online Fast',
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => const Scaffold(
              body: HomePage(),
            ),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/random': (context) => const Scaffold(
              body: RandomPage(),
            ),
        '/wordlist': (context) => const Scaffold(
              body: WordListPage(),
            ),
        '/revise': (context) => const Scaffold(
              body: RevisePage(),
            )
      },
      // home: MyHomePage(title: 'How to learn Russian Online Fast'),
    );
  }
}
